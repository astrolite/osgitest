package osgitest;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import osgitest.core;

public class OSGiActivator implements BundleActivator {

    @Override
    public void start(BundleContext bundleContext) throws Exception {
        ClassLoader clojureClassLoader = OSGiActivator.class.getClassLoader();
        ClassLoader priorClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(clojureClassLoader);
            osgitest.core.start(bundleContext);
        } finally {
            Thread.currentThread().setContextClassLoader(priorClassLoader);
        }
    }

    @Override
    public void stop(BundleContext bundleContext) throws Exception {
        ClassLoader clojureClassLoader = OSGiActivator.class.getClassLoader();
        ClassLoader priorClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(clojureClassLoader);
            osgitest.core.stop(bundleContext);
        } finally {
            Thread.currentThread().setContextClassLoader(priorClassLoader);
        }
    }

}
