(ns osgitest.core
  (:gen-class :name osgitest.core
              :methods [#^{:static true} [start [Object #_org.osgi.framework.BundleContext] void]
                        #^{:static true} [stop [org.osgi.framework.BundleContext] void]]
              :prefix "-"
              )
  (:import [org.apache.camel.impl DefaultCamelContext])
  (:import [org.apache.camel.core.osgi OsgiDefaultCamelContext])
  (:import [org.apache.camel.builder RouteBuilder])
  (:import org.apache.camel.Processor)
  (:require osgitest.tmp)
  )

(defonce camel_context (atom nil))

(defn -start [bcontext]
  (let [_ (if bcontext (.registerService bcontext "osgitest.core" (osgitest.core.) nil))
        _ (osgitest.tmp/xx)
        ccontext (if bcontext
                   (OsgiDefaultCamelContext. bcontext)
                   (DefaultCamelContext.))
        recv (proxy [org.apache.camel.Processor] []
               (process [exchange]
                 (-> exchange .getIn .getBody slurp prn)
                 (-> exchange .getOut (.setBody "Ыыыыы3333!"))))
        ]
    (.addRoutes ccontext (proxy [RouteBuilder] []
                          (configure []
                            ;; http://blog.christianposta.com/camel/easy-rest-endpoints-with-apache-camel-2-14/
                            ;; https://access.redhat.com/documentation/en-US/Red_Hat_JBoss_Fuse/6.2/html/Apache_Camel_Development_Guide/RestServices-RestDSL.html
                            (.. this
                                restConfiguration
                                (component "jetty" #_"servlet")
                                (contextPath "/")
                                (port 8086)
                                (bindingMode org.apache.camel.model.rest.RestBindingMode/auto)
                                )
                            #_(.. this (from "jetty:http://localhost:8088/myapp/myservice" #_ "file:/home/jw/scratch/inbox?noop=true")
                                (to "file:/tmp/outbox"))
                            (.. this (from "activemq:queue:start") (process recv))
                            (.. this (from "jetty://http://localhost:8087/xx")
                                #_(setHeader org.apache.camel.Exchange/HTTP_METHOD
                                           (org.apache.camel.language.constant.ConstantLanguage/constant "POST"))
                                ;;(setExchangePattern org.apache.camel.ExchangePattern/InOut)
                                ;;(process recv)
                                (to "activemq:queue:start")
                                )
                            (.. this
                                (rest "/say/hello")
                                get route transform (constant "Hello World get")
                                )
                            #_(.. this
                                (rest "/say/hello")
                                post route transform (constant "Hello World post")
                                )
                            )))
    (.start ccontext)
    (reset! camel_context ccontext)
    )


  (println :start bcontext)
  )
(defn -stop [bcontext]
  (println :stop bcontext)
  (if @camel_context (.stop @camel_context))
  )

(defn -main [& args]
  (-start nil)
  (.addShutdownHook (Runtime/getRuntime) (proxy [Thread] [] (run []
                                                              (prn :exit)
                                                              (-stop nil)))))
