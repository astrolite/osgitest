(defproject osgitest "0.1.0"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :main osgitest.core
  :manifest {"Bundle-SymbolicName" "osgitest"
             "Bundle-Name" "Osgitest"
             "Bundle-ManifestVersion" 2
             "Bundle-Localization" "plugin"
             "Bundle-Version" "0.1.0"
             "Bundle-Activator" "osgitest.OSGiActivator"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.osgi/org.osgi.core "4.2.0"]
                 [org.osgi/org.osgi.compendium "4.2.0"]

                 [org.apache.camel/camel-core "2.17.3"]
                 [org.apache.camel/camel-http "2.17.3"]
                 [org.apache.camel/camel-quartz "2.17.3"]
                 [org.apache.camel/camel-jetty "2.17.3"]
                 [org.apache.camel/camel-jetty9 "2.17.3"]
                 [org.apache.camel/camel-servlet "2.17.3"]
                 [org.apache.camel/camel-jdbc "2.17.3"]
                 [org.apache.camel/camel-core-osgi "2.17.3"]

                 [org.slf4j/jcl-over-slf4j "1.6.0"]
                 [org.slf4j/slf4j-api "1.6.0"]
                 [org.slf4j/slf4j-simple "1.6.0"]

                 [org.springframework/spring "2.5.6"]
                 [http-kit "2.1.18"]
                 ]
  :java-source-paths ["java"]
  :omit-source true
  ;;:jar-exclusions [#"clojure/osgi/.*"]
  :prep-tasks [["compile" "osgitest.core"] "javac" "compile"]
  :aot :all
  )
